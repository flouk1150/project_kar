const Koa = require("koa");
const Router = require("koa-router");
const serve = require("koa-static");
const mysql = require("mysql2/promise");
const path = require("path");
const render = require("koa-ejs");
const koaBody = require("koa-body");
const pool = require("./db");

const app = new Koa();
const router = new Router();

render(app, {
  root: path.join(process.cwd(), "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false
});

app.use(koaBody({ multipart: true }));

router.get("/", async (ctx, next) => {
  await ctx.render("template")
  await next();
});


app.use(serve(path.join(process.cwd(), "public")));
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(5000);
